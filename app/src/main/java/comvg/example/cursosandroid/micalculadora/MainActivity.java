package comvg.example.cursosandroid.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText txtUno, txtDos, txtResul;
    private Button btnSuma, btnResta, btnMulti, btnDivi, btnLimpiar, btnCerrar;
    private Operaciones op = new Operaciones();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponts();
    }

    public void initComponts() {
        txtUno = (EditText) findViewById(R.id.txtNum1);
        txtDos = (EditText) findViewById(R.id.txtNum2);
        txtResul = (EditText) findViewById(R.id.txtResul);
        txtResul = (EditText) findViewById(R.id.txtResul);

        btnSuma = (Button) findViewById(R.id.btnSuma);
        btnResta = (Button) findViewById(R.id.btnResta);
        btnMulti = (Button) findViewById(R.id.btnMult);
        btnDivi = (Button) findViewById(R.id.btnDivi);

        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        steEventos();
    }

    private void steEventos() {
        this.btnSuma.setOnClickListener(this);
        this.btnResta.setOnClickListener(this);
        this.btnMulti.setOnClickListener(this);
        this.btnDivi.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
        this.btnCerrar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSuma: //sumar
                if (!txtUno.getText().toString().isEmpty() && !txtDos.getText().toString().isEmpty()) {
                    op.setNum1(Float.parseFloat(txtUno.getText().toString().trim()));
                    op.setNum2(Float.parseFloat(txtDos.getText().toString().trim()));
                    txtResul.setText(Float.toString(op.suma()));
                }
                break;
            case R.id.btnResta: //restar
                if (!txtUno.getText().toString().isEmpty() && !txtDos.getText().toString().isEmpty()) {
                    op.setNum1(Float.parseFloat(txtUno.getText().toString().trim()));
                    op.setNum2(Float.parseFloat(txtDos.getText().toString().trim()));
                    txtResul.setText(Float.toString(op.resta()));
                }
                break;
            case R.id.btnMult: //multiplicar
                if (!txtUno.getText().toString().isEmpty() && !txtDos.getText().toString().isEmpty()) {
                    op.setNum1(Float.parseFloat(txtUno.getText().toString().trim()));
                    op.setNum2(Float.parseFloat(txtDos.getText().toString().trim()));
                    txtResul.setText(Float.toString(op.mult()));
                }
                break;
            case R.id.btnDivi: //dividir
                if (!txtUno.getText().toString().isEmpty() && !txtDos.getText().toString().isEmpty()) {
                    op.setNum1(Float.parseFloat(txtUno.getText().toString().trim()));
                    op.setNum2(Float.parseFloat(txtDos.getText().toString().trim()));
                    txtResul.setText(Float.toString(op.div()));
                }
                break;
            case R.id.btnLimpiar: //limpiar
                txtDos.setText("");
                txtUno.setText("");
                txtResul.setText("");
                break;
            case R.id.btnCerrar: //cerrar
                finish();
                break;
        }
    }
}
